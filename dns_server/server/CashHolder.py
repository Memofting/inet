import pickle
import sched
import time
import logging
import queue
import types
import os

from collections import defaultdict
from threading import RLock, Thread, Event
from itertools import chain
from functools import partial
from functools import wraps
from collections import namedtuple

from request.Packet import Packet
from server.DNSServer import DNSServer


CleanerDescriptor = namedtuple(
    "CleanerDescriptor", "delay priority start_time cash_name key value")

CLEANER_DESCRIPTOR_NAME = "cleaner_descriptor"
FILEDS_NAME = "_fields"


class PickableQueue(queue.Queue):
    EVENTS_NAME = "events"
    FINISH_TIME_NAME = "finish_time"

    def __init__(self):
        super().__init__()

    def __getstate__(self):
        q = []
        while True:
            try:
                q.append(self.get_nowait())
            except queue.Empty:
                break
        return q

    def __setstate__(self, state: list):
        self.__init__()

        cur_time = time.time()

        for e in state:
            diff = e.start_time + e.delay - cur_time
            if diff > 0:
                cd = CleanerDescriptor(
                    diff, e.priority, cur_time, e.cash_name, e.key, e.value)
                self.put_nowait(cd)


class SerializeFields:
    def __init__(self, *fields):
        self.fields = fields

    def __call__(self, func):
        @wraps(func)
        def wrapped(_self, *args, **kwargs):
            for field_name, field_type in self.fields:
                obj = getattr(_self, field_name, None)
                if obj is not None:
                    fname = field_name + ".pickle"
                    with open(fname, "wb") as fp:
                        pickle.dump(obj, fp, pickle.HIGHEST_PROTOCOL)
            return func(_self, *args, **kwargs)
        return wrapped

    def __get__(self, instance, cls):
        if instance is None:
            return self
        else:
            return types.MethodType(self, instance)


class DeserializeFields:
    def __init__(self, *fields):
        self.fields = fields

    def __call__(self, func):
        @wraps(func)
        def wrapped(_self, *args, **kwargs):
            for field_name, field_type in self.fields:
                fname = field_name + ".pickle"
                if os.path.exists(fname):
                    with open(fname, "rb") as fp:
                        obj = pickle.load(fp)
                        setattr(_self, field_name, obj)
                else:
                    setattr(_self, field_name, field_type())
            return func(_self, *args, **kwargs)
        return wrapped

    def __get__(self, instance, cls):
        if instance is None:
            return self
        else:
            return types.MethodType(self, instance)


class SerializeDeserializeFieldsMeta(type):
    def __new__(cls, name, bases, clsdict):
        clsobj = super().__new__(cls, name, bases, clsdict)
        clsobj = SerializeDeserializeFieldsMeta.serialize_deserialize_fields(
            clsobj)
        return clsobj

    @staticmethod
    def serialize_deserialize_fields(cls):
        names = getattr(cls, FILEDS_NAME, [])

        deleter = getattr(cls, "__del__", SerializeDeserializeFieldsMeta.empty)
        setattr(cls, "__del__", SerializeFields(*names)(deleter))

        initializer = getattr(cls, "__init__")
        setattr(cls, "__init__", DeserializeFields(*names)(initializer))

        return cls

    @staticmethod
    def empty(*args, **kwargs):
        pass


class CashHolder(metaclass=SerializeDeserializeFieldsMeta):
    STANDARD_PRIORITY = 1
    _fields = [
        ("name2IPv4_cash", partial(defaultdict, set)),
        ("authority_cash", partial(defaultdict, set)),
        ("name2IPv6_cash", partial(defaultdict, set)),
        ("IPv42name_cash", partial(defaultdict, set)),
        ("q", PickableQueue)
    ]
    dns_cashes_names = {
        b'\x00\x01': "name2IPv4_cash",
        b'\x00\x02': "authority_cash",
        b'\x00\x1c': "name2IPv6_cash",
        b'\x00\x0c': "IPv42name_cash"
    }

    def __init__(self, logger: logging.Logger):
        self.logger = logger
        self.scheduler = sched.scheduler(time.time, time.sleep)

        self.authority_cash[b''].add(DNSServer.ROOT_DNS_NAME)
        self.name2IPv4_cash[DNSServer.ROOT_DNS_NAME]\
            .add(DNSServer.ROOT_DNS_ADDRv4)

        self.rlock = RLock()
        self.cleaner = partial(
            self.remove_from_cash, rlock=self.rlock, logger=self.logger)
        self.ev = Event()
        self.t = Thread(target=self.start_scheduler, args=(
            self.cleaner, self.ev, self.q))
        self.t.start()

    @staticmethod
    def start_scheduler(func, ev: Event, q: queue.Queue):
        s = sched.scheduler(time.time)
        while not ev.is_set():
            next_ev = s.run(False)
            while True:
                try:
                    cd = q.get_nowait()
                except queue.Empty:
                    break
                delay = cd.delay
                priority = cd.priority
                s.enter(delay=delay, priority=priority, action=func,
                        kwargs={CLEANER_DESCRIPTOR_NAME: cd})
            if next_ev is not None:
                time.sleep(min(1, next_ev))
            else:
                time.sleep(1)
        else:
            for task in s.queue:
                kwargs = task.kwargs
                s.cancel(task)
                q.put_nowait(kwargs[CLEANER_DESCRIPTOR_NAME])

    def remove_from_cash(self, rlock: RLock, logger: logging.Logger,
                         *, cleaner_descriptor: CleanerDescriptor):
        logger.debug("start removing")
        cash_name = cleaner_descriptor.cash_name
        cash = getattr(self, cash_name, {})
        key = cleaner_descriptor.key
        value = cleaner_descriptor.value
        with rlock:
            s = cash.get(key)
            if s is not None and value in s:
                logger.debug(f"remove {value} from cash")
                s.remove(value)

    def retrieve_from_cash(self, name: bytes, qtype: bytes):
        cash_name = self.dns_cashes_names.get(qtype)
        if cash_name is not None:
            with self.rlock:
                cash_dict = getattr(self, cash_name)
                if cash_dict[name]:
                    self.logger.debug(f"cash matching: {name} {qtype} {cash_dict[name]}")
                    return cash_dict[name]

    def update_cash(self, packet: Packet):
        for rrs in chain(packet.answers, packet.authority, packet.additional):
            cash_name = self.dns_cashes_names.get(rrs.rtype)
            if cash_name is not None:
                with self.rlock:
                    cash_dict = getattr(self, cash_name)
                    cash_dict[rrs.name].add(rrs.rdata)
                    cd = CleanerDescriptor(
                        rrs.ttl, CashHolder.STANDARD_PRIORITY,
                        time.time(), cash_name, rrs.name, rrs.rdata)

                self.q.put_nowait(cd)

    def release_resources(self):
        with self.rlock:
            self.logger.debug("Cancel scheduled tasks and join thread: "
                              "(CashHolder.__del__)")
            self.ev.set()
            self.t.join()
            self.logger.debug("Stop waiting thread.")

