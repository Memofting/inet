import socket
import logging
import datetime
from collections import defaultdict
from itertools import chain

from request.Packet import Packet
from request.Question import Question
from request.functions import get_ip_from_bytes, encode_name, \
    get_packet, get_error_packet


class DNSServer:
    ROOT_DNS_NAME = b"l.root-servers.net"
    ROOT_DNS_ADDRv4 = b'\xc7\x07S*'
    TIMEOUT = 5

    def __init__(self, logger: logging.Logger, cash_holder, ip: str, port: int):
        # from server.CashHolder import CashHolder
        self.logger = logger
        self.ip, self.port = ip, port
        self.logger.debug(f"init dns server: {datetime.datetime.now()}")
        self.cash_holder = cash_holder
        # self.cash_holder = CashHolder(self.logger)

    def blocking_start(self):
        """
        This call process in one time only one request, so other are
        buffered and waited their turn. In the future this will be fixed.
        :return: None
        """
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.bind((self.ip, self.port))
        sock.settimeout(DNSServer.TIMEOUT)

        while True:
            try:

                data, addr = sock.recvfrom(1024)
            except socket.error as err:
                self.logger.debug(f"catch socket error (DNSServer.blocking_start): {err}")
            except KeyboardInterrupt:
                return
            else:
                self.logger.debug(f"recieved (DNSServer.blocking_start): {addr}: {data}\n")
                packet = Packet(data, self.logger)

                data_with_answers = self.resolve(data, packet)

                self.logger.debug(f"resolved data: {data_with_answers}")
                if data_with_answers is None:
                    data_with_answers = get_error_packet(packet.header.id)

                sock.sendto(data_with_answers, addr)

    def resolve(self, data: bytes, _packet: Packet):
        # todo: поддерживать работу для нескольких вопросов
        for question in _packet.questions:
            res = self.cash_holder.retrieve_from_cash(question.qname, question.qtype)
            if res is not None:
                mes = f"Get from cash for '{question.qname}'.\nAnswer: {res}\n"
                self.logger.debug(mes)
                return get_packet(_packet.header.id, question, 300, res)

            for searched_name in self.generate_fqdn(question.qname):
                server_names = self.cash_holder.authority_cash.get(searched_name, set())
                self.resolve_servers_addresses(server_names, question, _packet)

                server_addresses = list(filter(None, chain.from_iterable(
                    self.cash_holder.name2IPv4_cash[sn] for sn in server_names)))
                if not server_addresses:
                    server_addresses = (DNSServer.ROOT_DNS_ADDRv4,)

                response = self.try_get_response_from_servers(
                    server_addresses, data, _packet.header.id, question)
                if response is not None:
                    return response

    def try_get_response_from_servers(self, server_addresses: list, data: bytes,
                                      id: int, question: Question):
        for raw_address in server_addresses:
            server_address = get_ip_from_bytes(raw_address)

            sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            sock.settimeout(1)
            sock.sendto(data, (server_address, 53))
            self.logger.debug(f"send data to: {server_address}\n\n")
            try:
                ndata, _ = sock.recvfrom(1024)
            except socket.error:
                continue
            packet = Packet(ndata, self.logger)
            self.logger.debug(f" and packet {packet}")
            self.cash_holder.update_cash(packet)

            if packet.answers:
                return get_packet(id, question, 300,
                                  set(map(lambda e: e.rdata, packet.answers)))

    def generate_fqdn(self, name: bytes):
        # name = question.qname
        if name[-1] != b'.':
            name += b'.'

        splitted = name.split(b'.')
        part_name = []

        for part in splitted[::-1]:
            part_name.append(part)
            _n = b".".join(part_name[::-1])
            if _n:
                _n = _n[:-1]
            yield _n

    def resolve_servers_addresses(self, server_names: set,
                                  question: Question, packet: Packet):
        for server_name in server_names:
            if self.cash_holder.name2IPv4_cash.get(server_name): break
        else:
            for name in server_names:
                formatted_name = encode_name(name)
                q = Question(formatted_name + b'\x00\x01' + question.qclass, 0)
                _data = get_packet(packet.header.id, q, 300, set(),
                                   is_answer=False)
                _new_packet = Packet(_data, self.logger)

                self.resolve(_data, _new_packet)

    def __del__(self):
        self.logger.debug(f"delete DNSServer: (DNSServer.__del__)")
        del self.cash_holder
