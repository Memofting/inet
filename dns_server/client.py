import logging
import argparse
import dns.exception
import dns.rdtypes
import dns.resolver


def main(ip: str, port: str, question: str, qtype: str):
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)

    sh = logging.StreamHandler()
    sh.setLevel(logging.INFO)

    logger.addHandler(sh)

    resolver = dns.resolver.Resolver()
    resolver.nameservers = [ip]
    resolver.port = int(port)

    response = None
    try:
        response = resolver.query(question, qtype)
    except dns.exception.DNSException as exc:
        logger.info(f"dns error: {exc}")
    finally:
        sep = '\n\t'
        if response:
            logger.info(f"has response: \n\t{sep.join(map(str, response))}")
        else:
            logger.info(f"has not response")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Launch client for local dns server")
    parser.add_argument("ip", help="IP address of local dns server")
    parser.add_argument("port", help="PORT of local dns server")
    parser.add_argument("question", help="question for local dns server")
    parser.add_argument("qtype", help='Question type for local dns server')

    args = parser.parse_args()
    main(**vars(args))

