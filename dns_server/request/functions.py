import struct
from itertools import islice

from request.Question import Question


def parse_qname(packet: bytes, start: int):
        p = packet[start:]
        cnt = 0
        ip = iter(p)
        parts = []
        while True:
            length = next(ip)
            cnt += 1
            if length == 0:
                break
            if length >= 192:
                ptr = ((length & 0b00111111) << 8) + next(ip)
                cnt += 1
                name, _ = parse_qname(packet, ptr)
                parts.append(name)
                break
            else:
                name = bytes(islice(ip, length))
                cnt += length
                parts.append(name)
        return b".".join(parts).lower(), cnt


def get_ip_from_bytes(packet: bytes):
    return '.'.join(map(str, packet))


def encode_name(name: bytes):
    encoded = b''
    for e, l in map(lambda e: (e, len(e)), name.split(b'.')):
        encoded += struct.pack("!B", l)
        encoded += e
    encoded += b'\x00'
    return encoded


def encode_name_for_packet(name: bytes, type: bytes):
    # type A or AAAA
    if type == b'\x00\x01' or type == b"\x00\x1c":
        encoded = name
    # type NS or PTR
    else:
        encoded = encode_name(name)
    if type == b'\x00\05':
        a = True

    return encoded


def get_packet(id: int, rquestion : Question, rttl: int, answer_data: set,
               is_answer=True):
        rname = encode_name_for_packet(rquestion.qname, b'\x00\x02')
        rtype = rquestion.qtype
        rclass = rquestion.qclass
        rttl = struct.pack("!I", rttl)

        flags = bytearray((0b10000101, 0b00000000))
        if not is_answer:
            flags[0] &= 0b01111111

        # todo: работает без сжатия
        header = struct.pack("!H", id) \
                 + flags \
                 + b'\0\1' \
                 + struct.pack("!H", len(answer_data)) \
                 + b'\0\0\0\0'

        question = rname + rtype + rclass

        answers = []

        for data in answer_data:
            answer = [rname, rtype, rclass, rttl]
            # кодировать все равно придется, так как пакеты могут приходить не
            # с чистым именем, а содержать ссылки, которые просто так не
            # вставишь, на лету же делать имя в нужны формат, ну хз, надо ли...

            # if rtype == b"\x00\x0c":
            #     rdata = data
            # else:
            rdata = encode_name_for_packet(data, rtype)
            answer.append(struct.pack("!H", len(rdata)))
            answer.append(rdata)
            answers.append(b"".join(answer))

        return header + question + b''.join(answers)


def get_error_packet(id: int):
    flags = bytes((0b10000101, 0b00000010))

    # todo: работает без сжатия
    return struct.pack("!H", id) + flags + b'\0' * 8
