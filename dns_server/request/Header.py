import struct
from itertools import islice


class Header:
    __slots__ = [
        "id",
        "qr",  "opcode", "aa", "tc", "rd", "ra", "z", "rcode",
        "qdcount",
        "ancount",
        "nscount",
        "arcount"
    ]
    FORMAT = "!H2B4H"

    def __init__(self, data: bytes):
        l = struct.calcsize(Header.FORMAT)

        fields = struct.unpack(Header.FORMAT, bytes(islice(data, l)))
        self.id = fields[0]
        
        flags1 = fields[1]
        flags2 = fields[2]
        
        self.qr = flags1 & 0b10000000
        self.opcode = flags1 & 0b1111000
        self.aa = flags1 & 0b100
        self.tc = flags1 & 0b10
        self.rd = flags1 & 0b1
        
        self.ra = flags2 & 0b10000000
        self.z = flags2 & 0b1110000
        self.rcode = flags2 & 0b1111
        
        self.qdcount = fields[3]
        self.ancount = fields[4]
        self.nscount = fields[5]
        self.arcount = fields[6]

    def __str__(self):
        s = ""
        for name in self.__slots__:
            value = getattr(self, name)
            s += f"{name}: ({value}); "
        return s

    def __repr__(self):
        return str(self)

