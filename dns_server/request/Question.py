class Question:
    __slots__ = [
        'qname', 'qtype', 'qclass', 'shift'
    ]

    def __init__(self, packet: bytes, start: int):
        from request import functions
        self.qname, cnt = functions.parse_qname(packet, start)
        p = packet[start+cnt: start+cnt+4]
        self.qtype = p[:2]
        self.qclass = p[2:4]
        self.shift = start + cnt + 4

    def __str__(self):
        return f"qname: {self.qname}\n" \
               f"qtype: {self.qtype}\n" \
               f"qclass: {self.qclass}\n\n"

    def __repr__(self):
        return str(self)
