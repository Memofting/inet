import struct
import logging
from request.Header import Header
from request.Question import Question
from request.RRs import RRs


class Packet:
    __slots__ = [
        "header",
        "questions",
        "answers",
        "authority",
        "additional"
    ]

    def __init__(self, packet: bytes, logger: logging.Logger):

        header_len = struct.calcsize(Header.FORMAT)

        self.header = Header(packet)
        self.questions = []
        self.answers = []
        self.authority = []
        self.additional = []

        shift = header_len

        d = ((self.header.qdcount, Question, self.questions),
             (self.header.ancount, RRs, self.answers),
             (self.header.nscount, RRs, self.authority),
             (self.header.arcount, RRs, self.additional))

        for r, c, l in d:
            for _ in range(r):
                instance = c(packet, shift)
                shift = instance.shift
                l.append(instance)

    def __str__(self):
        sep = "\t\n"
        d = (("Questions", self.questions),
             ("Answers", self.answers),
             ("Authority", self.authority),
             ("Additional", self.additional))

        s = ""
        for n, l in d:
            s += f"""{n}: {sep.join(map(str, l))}\n"""

        return f"Header: {self.header}\n\n" + s

    def __repr__(self):
        return str(self)
