import struct
from request.functions import parse_qname


class RRs:
    __slots__ = [
        "name", 'rtype', 'rclass', 'ttl', 'rdlength', 'rdata', 'shift'
    ]
    FORMAT = "2HIH"

    def __init__(self, packet: bytes, start: int):
        self.name, cnt = parse_qname(packet, start)
        # self.name = self.name.decode()
        p = packet[start + cnt:]

        self.rtype = p[:2]
        self.rclass = p[2:4]
        self.ttl = struct.unpack("!I", p[4:8])[0]
        self.rdlength = struct.unpack("!H", p[8:10])[0]

        self.rdata = p[10:10+self.rdlength]

        # A and AAAA
        if self.rtype == b'\x00\x01' or self.rtype == b'\x00\x1c':
            self.rdata = p[10: 10 + self.rdlength]
        # NS
        else:
            self.rdata = parse_qname(packet, start + cnt + 10)[0]
        self.shift = start+cnt+10+self.rdlength

    def __str__(self):
        return f"name: {self.name}\n" \
               f"type: {self.rtype}\n" \
               f"class: {self.rclass}\n"\
               f"ttl: {self.ttl}\n" \
               f"rdlen: {self.rdlength}\n"\
               f"rdata: {self.rdata}\n"

    def __repr__(self):
        return str(self)

