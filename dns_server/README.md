# Кеширующий dns сервер

Код для простого кеширующего dns сервера. Поддерживает следующие типы записей: A, AAAA, PTR, NS

## Подготовка окружения

Перед запуском следует поставить все зависимости данного модуля для вашего интерпретатора:

```(bash)
user@host$ pip install -r requirements.txt
```

## Запуск сервера

Далее нужно запустить сервер на нужном вам адресе хоста и порте. Для понимания работы может пригодиться запуск в режиме дебага:

Следующей командой вы запустите сервер на адресе 127.0.0.1:8081 в режиме дебага:

```(bash)
user@host$ python main.py --ip 127.0.0.1 --port 8081 -d
```

## Общение с сервером

Так как делать запросы напрямую в сервер не очень удобно, рядом лежит клиент, который умеет делать запросы к нему из командной строки:

Он очень простой и принимает два аргумента question (QNAME в RFS или target domain name) и qtype (QTYPE в RFS или query type). Сейчас поддерживаются только 4 типа сообщений:

- A: Address Mapping record (A Record)—also known as a DNS host record, stores a hostname and its corresponding IPv4 address.
- NS: Name Server records (NS Record)—specifies that a DNS Zone, such as “example.com” is delegated to a specific Authoritative Name Server, and provides the address of the name server.
- PTR: Reverse-lookup Pointer records (PTR Record)—allows a DNS resolver to provide an IP address and receive a hostname (reverse DNS lookup).
- AAAA: IP Version 6 Address record (AAAA Record)—stores a hostname and its corresponding IPv6 address.

### Пример с A записью

Например, так можно получить ip адресс для домена `e1.ru`:

```(bash)
name@host$ python client.py 127.0.0.1 8081 e1.ru A
```

Вот такой ответ можно получить:
```(bash)
has response: 
        195.19.220.24
```

### Пример с AAAA записью

А так получить IPv6 адрес сайта google.com:

```(bash)
name@host$ python client.py 127.0.0.1 8081 google.com AAAA
```

И вот такой ответ получить:

```(bash)
has response: 
        2a00:1450:4010:c03::66
        2a00:1450:4010:c03::64
        2a00:1450:4010:c03::71
        2a00:1450:4010:c03::8b
```

### Про отключение интернета

Так как dns сервер кеширующий, то можно попробовать сначала получить ip адресс домента. Выключить интернет и потом попробовать получить название dns зон:

Запрашиваем ip адресс:

```(bash)
name@host$ python client.py 127.0.0.1 8081 google.com A
has response: 
        209.85.233.100
        209.85.233.138
        209.85.233.101
        209.85.233.139
        209.85.233.102
        209.85.233.113
```

Затем выключаем wifi

А затем запрашиваем NS записи:

```(bash)
name@host$ python client.py 127.0.0.1 8081 google.com NS
has response: 
        ns1.google.com.
        ns4.google.com.
        ns3.google.com.
        ns2.google.com.
```

Ура!!! Кеширование работает.