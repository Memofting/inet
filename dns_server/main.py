import logging
import argparse
from contextlib import contextmanager

from server.CashHolder import CashHolder
from server.DNSServer import DNSServer

IPv4 = "127.0.0.3"
PORT = 5050


def get_logger(log_level: int):
    logger = logging.Logger(__name__)
    logger.setLevel(log_level)

    fh = logging.FileHandler("log.log")
    fh.setLevel(log_level)

    sh = logging.StreamHandler()
    sh.setLevel(log_level)

    logger.addHandler(fh)
    logger.addHandler(sh)

    return logger


# @contextmanager
# def managed_resource(*args, **kwds):
#     # Code to acquire resource, e.g.:
#     resource = acquire_resource(*args, **kwds)
#     try:
#         yield resource
#     finally:
#         # Code to release resource, e.g.:
#         release_resource(resource)

@contextmanager
def aquire_cash_holder(logger: logging.Logger):
    cash_holder = CashHolder(logger)

    try:
        yield cash_holder
    finally:
        cash_holder.release_resources()


def main(*, ip: str, port: str, log_level: int, **kwargs):
    logger = get_logger(log_level)

    with aquire_cash_holder(logger) as cash_holder:
        dns_server = DNSServer(logger, cash_holder, ip, int(port))
        try:
            dns_server.blocking_start()
        except KeyboardInterrupt:
            pass


def get_args():
    parser = argparse.ArgumentParser(
        description="This script launched dns server on desired "
                    "loopback address and desired port. "
                    "For system ports required super user's rights.",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("--ip", default=IPv4)
    parser.add_argument("--port", type=int, default=PORT)
    parser.add_argument('-d', "--debug", action="store_true",
                        help="enable debug mode for logging.")
    parser.add_argument('-i', "--info", action="store_true",
                        help='enable info mode for logging.')

    args = parser.parse_args()
    d = dict(vars(args))
    d["log_level"] = logging.ERROR
    if args.debug:
        d["log_level"] = logging.DEBUG
    if args.info:
        d["log_level"] = logging.INFO
    return d


if __name__ == "__main__":
    # import gc

    d = get_args()
    main(**d)
    # gc.collect()

