import enum


class Protocols(enum.Enum):
    TCP = "TCP"
    UDP = "UDP"

    def __repr__(self):
        return str(self)

    def __str__(self):
        return self.value
