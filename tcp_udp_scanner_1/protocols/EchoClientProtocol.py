import asyncio


class EchoClientProtocol(asyncio.Protocol):
    ATTEMPTS = 3

    def __init__(self, loop, logger, port):
        self.loop = loop
        self.logger = logger
        self.port = port
        self.transport = None
        self.port_open = loop.create_future()
        self.attempts = EchoClientProtocol.ATTEMPTS
        self.loop.call_later(1, self.try_once_or_cancel)

    def connection_made(self, transport):
        self.logger.debug(f"Connection made! with {self.port}")
        self.transport = transport
        self.transport.sendto(b"1")

    def datagram_received(self, data, addr):
        self.logger.debug(f"Received: {data.decode()} from {addr}")
        self.logger.debug("Close the socket")
        self.transport.close()
        self.port_open.set_result(True)

    def error_received(self, exc):
        if not self.port_open.done():
            self.logger.debug(f"Error received: {exc} with {self.port}")
            self.port_open.set_result(False)

    def connection_lost(self, exc):
        if not self.port_open.done():
            self.logger.debug(f"Connection closed with {self.port}")
            self.port_open.set_result(False)

    def try_once_or_cancel(self):
        self.attempts -= 1
        if not self.attempts:
            if not self.port_open.done():
                self.port_open.set_result(True)
        else:
            self.loop.call_later(1, self.try_once_or_cancel)
