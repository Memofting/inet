import asyncio
import socket
import logging
from collections import defaultdict
from functools import partial
from protocols.EchoClientProtocol import EchoClientProtocol
from protocols.Protocols import Protocols


def get_logger(filename, is_debug):
    logger = logging.getLogger('simple_example')
    ch = logging.FileHandler(filename)

    if is_debug:
        logger.setLevel(logging.DEBUG)
        ch.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)
        ch.setLevel(logging.INFO)

    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    ch.setFormatter(formatter)

    logger.addHandler(ch)
    return logger


async def create_tcp_connection(host, port, ports, loop, logger):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.setblocking(False)
    sock.settimeout(5)
    try:
        await loop.sock_connect(sock, (host, port))
    except ConnectionRefusedError or socket.gaierror or socket.error:
        pass
    else:
        ports.append(port)
    finally:
        print(port)
        sock.close()


async def create_udp_connection(host, port, ports, loop, logger):
    transport, protocol = await loop.create_datagram_endpoint(
        lambda: EchoClientProtocol(loop, logger, port),
        remote_addr=(host, port))
    logger.debug(f"using port: {port}")
    try:
        res = await protocol.port_open
        if res:
            ports.append(port)
    finally:
        transport.close()


def check_ports(client, protocol, ports_range):
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    ports = []
    clients = []
    for port in ports_range:
        clients.append(client(port, ports, loop))

    try:
        loop.run_until_complete(asyncio.gather(*clients))
    finally:
        loop.close()
    return (protocol, ports)


def main(step=3000):
    from multiprocessing import Pool

    args = get_args_parser()
    logger = get_logger(args.logfile, args.debug)
    tcp_client = partial(create_tcp_connection, args.ip, logger=logger)
    udp_client = partial(create_udp_connection, args.ip, logger=logger)

    d = {
        Protocols.TCP: tcp_client,
        Protocols.UDP: udp_client,
    }

    port_ranges = []
    start, end = args.start, args.end
    while end > start:
        port_ranges.append(tuple(range(start, min(start + step, end))))
        start += step

    process_args = []
    for p in args.protocols:
        client = d[p]
        for r in port_ranges:
            process_args.append((client, p, r))

    # pool = Pool(5)
    # res = pool.starmap(check_ports, process_args)

    # ans = defaultdict(list)
    # for port, ports in res:
    #     ans[port].extend(ports)
    ans = check_ports(*process_args[0])
    print(*ans)
    # logger.debug(ans)

    # for port, ports in ans.items():
    #     print(port, ", ".join(map(str, sorted(ports))), sep=": ")


def get_args_parser():
    import argparse

    desc = "This program scans TCP/UDP ports of the host."
    tcp_help = "Look for opened TCP ports (by default)"
    udp_help = "Look for opened UDP ports"
    log_help = "Name of the file for logging"
    debug_help = "Use logging"
    filename = "example.log"

    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument("ip", help="IP address of desired host.")
    parser.add_argument("start", type=int,
                        help="Start of port's ranges (including).")
    parser.add_argument("end", type=int,
                        help="End of port's ranges (not including).")

    parser.add_argument("-t", "--tcp", action="append_const", dest="protocols",
                        help=tcp_help, const=Protocols.TCP)
    parser.add_argument("-u", "--udp", action="append_const", dest="protocols",
                        help=udp_help, const=Protocols.UDP)
    parser.add_argument("-d", "--debug", action="store_true",
                        help=debug_help)
    parser.add_argument("-l", "--logfile", help=log_help, default=filename)

    args = parser.parse_args()
    if not args.protocols:
        args.protocols = [Protocols.TCP]

    assert args.end > args.start

    return args


if __name__ == "__main__":
    main()

