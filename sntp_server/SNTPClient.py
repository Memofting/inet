import socket
import struct
import time
SNTP_HOST = "127.0.0.1"
SNTP_PORT = 123
TIME1970 = 2208988800


def sntp_client():
    client = socket.socket( socket.AF_INET, socket.SOCK_STREAM)
    try:
        data = '\x1b' + 47 * '\0'
        client.connect((SNTP_HOST, SNTP_PORT))
        client.sendall(data.encode('utf-8'))
        data = client.recv(1024)
    finally:
        client.close()
    if data:
        print('Response received from:', SNTP_HOST)
    t = struct.unpack('!12I', data)[10]
    # t -= TIME1970
    print('\tTime=%s' % time.ctime(t))


if __name__ == '__main__':
    sntp_client()
