import struct


class SNTPPacket:
    SIZE = 48
    LI = 0b11000000
    VN = 0b111000
    MODE = 0b111
    FORMAT = "!4B3I4Q"

    def __init__(self, packet: bytes):
        data = struct.unpack(SNTPPacket.FORMAT, packet)
        b1 = data[0]
        self.li = 0b0
        self.vn = b1 & SNTPPacket.VN
        self.mode = 0b100

        self.stratum = data[1]
        self.poll = data[2]
        self.precision = data[3]

        self.root_delay = 0
        self.dispersion = 0
        self.identifier = struct.unpack("!I", b"LOCL")[0]

        self.reference_ts = data[7]
        self.originate_ts = data[8]
        self.receive_ts = data[9]
        self.transmit_ts = data[10]

    def __bytes__(self):
        data = [self.li << 6 + self.vn << 3 + self.mode]
        data.extend(
            [
                self.stratum,
                self.poll,
                self.precision,
                self.root_delay,
                self.dispersion,
                self.identifier,
                self.receive_ts,
                self.originate_ts,
                self.receive_ts,
                self.transmit_ts
            ]
        )
        return struct.pack(SNTPPacket.FORMAT, *data)


class SNTPServer:
    def __init__(self, conf_file_name: str = ".conf"):
        self.internal_delay = 0
        with open(conf_file_name) as fp:
            self.internal_delay = int(fp.readline().strip())

    def reply_on(self, packet: bytes):
        sntp_pakcet = SNTPPacket(packet)
        sntp_pakcet.originate_ts = sntp_pakcet.transmit_ts
        sntp_pakcet.receive_ts = self.get_cur_time_in_ntp_format()
        sntp_pakcet.transmit_ts = self.get_cur_time_in_ntp_format()

        return bytes(sntp_pakcet)

    def get_cur_time_in_ntp_format(self):
        import time
        t = time.time() + self.internal_delay
        return int(t // 1) << 32


def main(host="127.0.0.1", port=123, data_payload=2048):
    import socket
    sntp_server = SNTPServer()
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.bind((host, port))
        sock.listen(1)
        while True:
            client, address = sock.accept()
            try:
                print(f"Accepted from {address}")
                data = client.recv(data_payload)
                print(data)

                if len(data) < SNTPPacket.SIZE:
                    client.sendall(b"No")
                    continue

                # while len(data) < SNTPPacket.SIZE:
                #     data += client.recv(data_payload)
                # print(f"Current data: {data}\nData len: {len(data)}")
                packet = sntp_server.reply_on(data[:SNTPPacket.SIZE])
                print(packet)
                client.sendall(packet)
            finally:
                client.close()
    finally:
        sock.close()


if __name__ == "__main__":
    main()
