import argparse


def main():
    from SNTPServer import main as launch_server

    parser = argparse.ArgumentParser(
        description="Launch sntp server. It launched on 127.0.0.1:123 TCP.")
    parser.parse_args()

    launch_server()


if __name__ == "__main__":
    main()
