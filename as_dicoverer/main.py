import io
import re
from subprocess import Popen, PIPE
from ipwhois import IPWhois, exceptions


ASN = "asn"
ASN_DESCRIPTION = "asn_description"
ATTEMPTS = 3


# todo: Добавить опции запуска -I, -T, -U + requrements
def generate_as(ip):
    command = f"traceroute -I {ip}"

    with Popen(command,
               shell=True, stdout=PIPE, stderr=PIPE) as proc:
        cnt = 1
        attempt = 0
        for router_result in io.TextIOWrapper(proc.stdout, encoding="utf-8"):
            mo = re.search(r"(?a:(?P<ip>\d{1,3}(?:\.\d{1,3}){1,3}))", router_result)
            if mo is None:
                attempt += 1
                if attempt == ATTEMPTS:
                    break
                continue
            attempt = 0
            ip = mo["ip"]
            try:
                obj = IPWhois(ip)
                d = obj.lookup_rdap()
                yield cnt, ip, d[ASN], d[ASN_DESCRIPTION]
                cnt += 1
            except exceptions.BaseIpwhoisException:
                continue


def main():
    import argparse
    from collections import namedtuple

    table_line = namedtuple("table_line", "cnt ip asn desc")

    parser = argparse.ArgumentParser(
        description="Launch route tracing and show passed AS.")
    parser.add_argument("ip", help="Desired ip address.")

    args = parser.parse_args()
    for items in map(lambda e: table_line(*e), generate_as(args.ip)):
        print("{i.cnt:<4d}{i.ip:20}{i.asn:15}{i.desc}".format(i=items))


if __name__ == "__main__":
    main()
