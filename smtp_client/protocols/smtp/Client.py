import io
import logging
import socket
from pathlib import Path

from functions.Conf import Conf
from functools import wraps, partial


def read_fd(fd, logger: logging.Logger):
    def ans_parser(ans: str):
        return int(ans[:3]), ans[4:]

    # так мы не заблокируем поток выполнения
    reply: bytes = fd.read1(1024)

    ans = None
    for line in io.TextIOWrapper(io.BytesIO(reply)):
        line = line.strip()
        logger.debug(f"reply: {line}")
        ans = ans_parser(line)
    return ans


def log_out(logger: logging.Logger, fd, func):
    @wraps(func)
    def wrapped(data, *args, **kwargs):
        data = bytes(data + "\r\n", "ascii")
        logger.debug(f"sent: {data}")
        r = func(data, *args, **kwargs)
        return read_fd(fd, logger)

    return wrapped


class Client:
    """
    Класс клиента, который обеспечивает передачу данных по протоколу SMTP
    вместе с их шифрованием по протоколу TLS.
    """
    def __init__(self, host_name: str, host_port: int,
                 *, folder_path: Path,
                 user_email: str, user_password: str,
                 conf: Conf, logger: logging.Logger):
        self.host_ip = socket.gethostbyname(host_name)
        self.host_port = host_port
        self.host_name = host_name
        self.user_email = user_email
        self.user_password = user_password
        self.conf = conf
        self.logger = logger

        self.folder_path = folder_path
        self._host = socket.gethostbyname(socket.gethostname())

    def send(self):
        import socket
        import ssl
        from email.base64mime import body_encode as encode_base64
        from email.base64mime import body_decode as decode_base64

        context = ssl.SSLContext()
        conn = socket.create_connection((self.host_name, self.host_port))
        inp = conn.makefile(mode='br')
        read_fd(inp, self.logger)

        send = log_out(self.logger, inp, conn.sendall)

        code, mes = send(f"ehlo [{self._host}]")
        code, mes = send("STARTTLS")
        if code == 220:
            conn = context.wrap_socket(conn, server_hostname=self._host)
            inp = conn.makefile(mode='br')
            send = log_out(self.logger, inp, conn.sendall)
        else:
            raise IOError("SMTP server doesn't support STARTTLS")
        code, mes = send(f"ehlo [{self._host}]")

        email = encode_base64(f"{self.user_email}".encode(), eol='')
        password = encode_base64(f"{self.user_password}".encode(), eol='')

        code, mes = send(f"AUTH LOGIN")
        self.logger.debug(f"received: {decode_base64(mes)}")
        code, mes = send(f"{email}")
        self.logger.debug(f"received: {decode_base64(mes)}")
        code, mes = send(f"{password}")

        code, mes = send(f"MAIL FROM:<{self.user_email}>")
        for receiver in self.conf.Recipients:
            code, mes = send(f"RCPT TO:<{receiver}>")
        code, mes = send("DATA")

        message = self._get_message()

        code, mes = send(message)

        code, mes = send(f"quit")

    def _get_message(self):
        from os.path import basename
        from email.mime.multipart import MIMEMultipart
        from email.mime.application import MIMEApplication
        from email.mime.text import MIMEText

        msg = MIMEMultipart()

        msg['From'] = self.user_email
        msg['To'] = self.conf.Recipients[0]
        msg["Subject"] = self.conf.Theme

        mes_file = self.folder_path / self.conf.MessageFile
        with open(mes_file) as fd:
            msg.attach(MIMEText(fd.read()))

        for file_name in self.conf.Attachments:
            relative_file_name = self.folder_path / file_name
            with open(relative_file_name, "rb") as fd:
                bn = basename(relative_file_name)
                part = MIMEApplication(fd.read(), Name=bn)
                content_disposition = f'attachment; filename="{bn}"'
                part["Content-Disposition"] = content_disposition
                msg.attach(part)
        return msg.as_string() + "\r\n."

