class Conf:
    __slots__ = {"Recipients", "Theme", "Attachments", "MessageFile"}

    def __init__(self, kwargs):
        for n in Conf.__slots__:
            setattr(self, n, None)
        for k, v in kwargs.items():
            setattr(self, k, v)
