# 465 порт с шифрованием, 587 с tls

owner2smtp = {
    "yandex": ("smtp.yandex.ru", 587),
    "google": ("smtp.gmail.com", 587),
    "mail.ru": ("smtp.mail.ru", 587)
}


def get_smtp_host_port(mail_address: str):
    import dns.resolver
    from ipwhois import IPWhois

    resolver = dns.resolver.Resolver()
    domain_mail = mail_address[1+mail_address.rfind("@"):]

    dns_response = resolver.query(domain_mail, "MX")

    responses = []
    for response in dns_response:
        v, n = str(response).split()
        responses.append((int(v), n))
    print(responses)
    sorted(responses, reverse=True)
    host_name = responses[0][1]

    domain_ip = list(map(str, resolver.query(host_name, "A")))[0]

    obj = IPWhois(domain_ip)
    d = obj.lookup_rdap()
    owner: str = d["asn_description"]
    owner = owner.lower()
    for k, v in owner2smtp.items():
        if k in owner:
            return v
