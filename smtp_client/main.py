import sys
import logging
from pathlib import Path
from functions.functions import *
from functions.Conf import Conf
from protocols.smtp.Client import Client

IO_ERROR_CODE = 2


def main(*, folder: str, logger: logging.Logger, email, password, **kwargs):
    folder_path = Path(folder)
    conf_path = folder_path / "conf.json"
    if conf_path.exists():
        with open(conf_path, "rb") as fp:
            import json
            conf = json.load(fp, object_hook=Conf)
            host_name, host_port = get_smtp_host_port(email)
            logger.debug(f"email address: {email}\n"
                         f"host name: {host_name}\n"
                         f"host port: {host_port}\n\n")
            client = Client(host_name, host_port, folder_path=folder_path,
                            user_email=email, user_password=password,
                            conf=conf, logger=logger)
            client.send()
    else:
        logger.debug(f"Not such file or directory: {conf_path}")
        sys.exit(IO_ERROR_CODE)


if __name__ == "__main__":
    import argparse
    import getpass
    import logging

    parser = argparse.ArgumentParser(
        description="Launch local smtp client")
    parser.add_argument("-d", "--debug", action="store_true")
    parser.add_argument("folder", help="Path to the folder with \"conf.json\" file")
    parser.add_argument("email", help="User email address")
    args = parser.parse_args()

    level = logging.ERROR
    if args.debug:
        level = logging.DEBUG
    logger = logging.getLogger(__name__)
    logger.setLevel(level)

    sh = logging.StreamHandler()
    sh.setLevel(level)
    logger.addHandler(sh)

    password = getpass.getpass("Enter your Password: ")

    args_d = {"logger": logger, "password": password}
    args_d.update(vars(args))

    main(**args_d)

