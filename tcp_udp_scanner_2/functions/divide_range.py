def divide_range(start, end, step=None):
    if step is None:
        import resource
        step = resource.getrlimit(resource.RLIMIT_NOFILE)[0]
    step //= 2
    for i in range(start, end, step):
        yield i, min(i + step, end)