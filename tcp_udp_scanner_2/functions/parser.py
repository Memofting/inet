import socket
import logging
from operator import methodcaller


HTTP = "http"
SMTP = "smtp"
POP3 = "pop3"

DNS = "dns"
SNTP = "sntp"

HTTP_REQUEST = b"GET/HTTP/1.1\r\n"
SMTP_REQUEST = b"HELO"

HTTP_RESPONSE = b"HTTP"
SMTP_RESPONSE = b"220"
POP3_RESPONSE = b"+OK"

DNS_REQUEST = b"AA\1\0\0\1\0\0\0\0\0\0\7example\3com\0\0\1\0\1"
DNS_RESPONSE = b"AA"

SNTP_REQUEST = b'\x1b' + 43 * b'\0' + b"AAAA"
SNTP_RESPONSE = b"AAAA"

is_dns = methodcaller("startswith", DNS_RESPONSE)
is_sntp = lambda e: len(e) > 20 and e[-20:-16] == SNTP_RESPONSE

is_http = methodcaller("startswith", HTTP_RESPONSE)
is_smtp = methodcaller("startswith", SMTP_RESPONSE)
is_pop3 = methodcaller("startswith", POP3_RESPONSE)

tcp_proto = {
    HTTP: (HTTP_REQUEST, is_http, 4),
    SMTP: (HTTP_REQUEST, is_smtp, 3),
    POP3: (HTTP_REQUEST, is_pop3, 3),
    SNTP: (SNTP_REQUEST, is_sntp, 48)
}

udp_proto = {
    DNS: (DNS_REQUEST, is_dns, 2),
    SNTP: (SNTP_REQUEST, is_sntp, 48)
}


def parse_tcp(ip: str, port: int, timeout: int, *, logger, **kwargs):
    logger.debug(parse_tcp.__name__)

    for name, (request, is_proto, resp_len) in tcp_proto.items():
        logger.debug(f"{name}, {request}, {resp_len}")

        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        sock.setblocking(True)
        sock.settimeout(timeout)
        sock.connect((ip, port))

        try:
            data = b''
            sock.sendall(request)

            try:
                data = sock.recv(1024)
            except socket.error as err:
                print(f"error: {name} {port}: {err}")
            finally:
                print(f"\n{name} {port}:\ndata: {data}")
                logger.debug(f"{name}: {data}")

            if is_proto(data):
                return name
        finally:
            sock.close()


def parse_udp(ip: str, port: int, timeout: int, *, logger, **kwargs):
    logger.debug(parse_udp.__name__)
    for name, (request, is_proto, resp_len) in udp_proto.items():
        logger.debug(f"{name}, {request}, {resp_len}")

        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.setblocking(True)
        sock.settimeout(timeout)

        try:
            data = b''
            try:
                sock.sendto(request, (ip, port))
                data, _ = sock.recvfrom(1024)
                if is_proto(data):
                    return name
            except socket.error as err:
                logger.debug(f"{name}: {err}")
            finally:
                logger.debug(f"{name} {data}")
        finally:
            sock.close()


__all__ = ["parse_udp", "parse_tcp"]
