import socket

# function for executing in different thread
def icmp_server():
    sock_icmp = socket.socket(socket.AF_INET,
        socket.SOCK_RAW, socket.IPPROTO_ICMP)
    sock_icmp.setsockopt(socket.SOL_IP, socket.IP_HDRINCL, 1)

    while 1:
        addr, data = sock_icmp.recvfrom(1024)
        print(f"{addr}:     {data}")

icmp_server()
