import socket

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

sock.sendto(b'hello, world!', ("8.8.8.8", 6060))
data = sock.recvfrom(2)

print(data)
sock.close()
