import logging
from protocols.tcp.scanner import tcp_main
from protocols.udp.scanner import udp_main


def main(*, udp=True, tcp=True, **kwargs):
    if udp:
        udp_main(**kwargs)
    if tcp:
        tcp_main(**kwargs)


if __name__ == "__main__":
    import argparse

    desc = "Parse opened tcp/udp ports on the host in the specified range."

    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument("start", type=int)
    parser.add_argument("end", type=int)
    parser.add_argument("-i", "--ip", default="127.0.0.1")
    parser.add_argument("-t", "--timeout", type=int, default=1)
    parser.add_argument("--tcp", action="store_true")
    parser.add_argument("--udp", action="store_true")
    parser.add_argument("-l", "--logging", action="store_true")

    args = parser.parse_args()
    if not (args.tcp or args.udp):
        args.tcp = True

    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)

    kwargs = dict(vars(args))
    kwargs["logger"] = logger

    if args.logging and not args.logging:
        logger.setLevel(logging.DEBUG)
        fh = logging.FileHandler("log.log")
        fh.setLevel(logging.DEBUG)

        logger.addHandler(fh)

    main(**kwargs)
