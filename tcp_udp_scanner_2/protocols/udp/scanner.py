import socket
import time
import struct
from multiprocessing import Pool, Event
from multiprocessing.pool import ThreadPool
from collections.abc import Iterable
from functools import partial
from functions.divide_range import divide_range
from functions.parser import parse_udp
from protocols.icmp.server import icmp_server


def udp_main(start, end, **kwargs):
    args=[]
    for s, e in divide_range(start, end):
       args.append((s, e))
    p = Pool()
    ev = Event()
    pool = ThreadPool(1)
    async_res = pool.apply_async(icmp_server, args=(ev,))
    ans=set(range(start, end))
    for part_ans in p.starmap(partial(udp_scanner, **kwargs), args):
       if isinstance(part_ans, Iterable):
           ans.update(part_ans)
    ev.set()
    for_delete=async_res.get()
    ports = ans.difference(for_delete)

    ans = []
    for port in ports:
        name = parse_udp(port=port, **kwargs)
        ans.append(f"{port}: ({name})")

    print('UDP:\n\t', "\n\t".join(ans), sep='')


def udp_scanner(start, end, *, ip, timeout, **kwargs):
    sockets = {}

    ports = list(range(start, end))
    for port in ports:
        sock_udp = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock_udp.setblocking(False)
        try:
            sock_udp.sendto(struct.pack("I", port), (ip, port))
        except socket.error:
            sock_udp.close()
            continue
        sockets[sock_udp] = port

    time.sleep(timeout)
    return ports
