import socket
import time
from multiprocessing import Pool
from collections.abc import Iterable
from functools import partial
from functions.divide_range import divide_range
import functions.parser as parser


def tcp_main(start, end, **kwargs):
    args = []
    for s, e in divide_range(start, end):
        args.append((s, e))
    p = Pool()
    ans = []
    for part_ans in p.starmap(partial(tcp_scanner, **kwargs), args):
        if isinstance(part_ans, Iterable):
            ans.extend(part_ans)
    print("TCP:\n\t", "\n\t".join(ans), sep='')


def tcp_scanner(start, end, *, ip, timeout, **kwargs):
    sockets = {}

    for port in range(start, end):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.setblocking(False)

        sock.connect_ex((ip, port))
        sockets[sock] = port

    time.sleep(timeout)

    ports = []
    for sock in sockets:
        port = sockets[sock]

        opt = sock.connect_ex((ip, port))
        if opt == 0:
            ports.append(port)
        sock.close()
    ans = []
    for port in ports:
        name = parser.parse_tcp(ip, port, timeout, **kwargs)
        ans.append(f"{port}: ({name})")

    return ans
