import socket
import struct
from multiprocessing import Event


# function for executing in different thread
def icmp_server(ev: Event):
    sock_icmp = socket.socket(socket.AF_INET, socket.SOCK_RAW,
                              socket.IPPROTO_ICMP)
    sock_icmp.setsockopt(socket.SOL_IP, socket.IP_HDRINCL, 1)
    sock_icmp.setblocking(False)

    for_delete = []
    while not ev.is_set():
        try:
            data, addr = sock_icmp.recvfrom(1024)
        except socket.error:
            pass
        else:
            # skip ip header, icmp packet, inner ip
            # and udp headers and read port data
            port = struct.unpack("I", data[20 + 8 + 20 + 8:])[0]
            for_delete.append(port)
    return for_delete
